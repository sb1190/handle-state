import { BehaviorSubject, Observable } from "rxjs";
import { scan, shareReplay } from "rxjs/operators";

export class GlobalState<T> {

    private readonly state: BehaviorSubject<T>;
    public state$: Observable<T>;

    constructor() {

        this.state = new BehaviorSubject({} as T);
        this.state$ = this.state.asObservable().pipe(
            scan((acc: T, newVal: T) => ({
                ...acc,
                ...newVal,
            })),
            shareReplay(1)
        );
    }

    public dispatch(partialState: Partial<T>): void {
        const actualState = this.state.getValue();
        this.state.next({ ...actualState, ...partialState });
    }

    public getStateValue(): T {
        return this.state.getValue();
    }

}

