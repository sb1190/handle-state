import { EventBus, EventBusMessageType } from './event-bus';
import { GlobalState } from './global-state';

// Example global state

interface State { userName: string; }

const globalState = new GlobalState<State>();

function componentGetsUserInformation(): void {
    // save token user
    globalState.dispatch({ userName: "demo123456789" });
}

function navbarComponent(): void {
    // get Token user
    globalState.state$.subscribe((state: State) =>
        console.log(state, "Get state in component two")
    );
}

function userProfileComponent(): void {
    // get Token user
    globalState.state$.subscribe((state: State) =>
        console.log(state, "Get state in component three")
    );
}

componentGetsUserInformation();
navbarComponent();
userProfileComponent();

// Example event bus

const eventBus = new EventBus();

interface Product { id: string; name: string; }

function buttonComponent(): void {
    // Emit event when user clicks
    eventBus.cast<Product>({ key: "clickEvent", data: { id: '123456789', name: "Demo" } });
}

function componentReactToButtonClickEvent(): void {
    // listener click Event 
    eventBus.on("clickEvent").subscribe((event: EventBusMessageType<Product>) =>
        console.log(event, "Get state in component three")
    );
}

componentReactToButtonClickEvent();
buttonComponent();