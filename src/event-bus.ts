import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export interface EventBusMessageType<T> {
    key: string;
    data?: T;
}

export class EventBus {
    private eventBus: Subject<EventBusMessageType<unknown>>;

    constructor() {
        this.eventBus = new Subject<EventBusMessageType<unknown>>();
    }

    public cast<T>({ key, data }: EventBusMessageType<T>): void {
        this.eventBus.next({ key, data });
    }

    public on<T>(key: string): Observable<EventBusMessageType<T>> {
        return this.eventBus.asObservable().pipe(
            filter((event: EventBusMessageType<unknown>): boolean => event.key === key),
            map((event: EventBusMessageType<unknown>): EventBusMessageType<T> => event as EventBusMessageType<T>)
        );
    }
}